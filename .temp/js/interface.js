/* base */
(function() {
	$(document).delegate('.toggle-item .btn-toggle','click',function(){
        var $btn    = $(this);
        var $item   = $btn.parents('.toggle-item');
        $item.toggleClass('toggled').find('.toggle-item-in').slideToggle(300);
        return false; 
    });
})();
/* gallery */
(function() {
  var init = function(){
  	$('.gallery').each(function(){
  		var $gallery 	= $(this);
  		var $slider 	= $gallery.find('.carousel');
  		var $slides 	= $slider.find('.slide');
  		var $thumbs 	= $gallery.find('.thumbs');
  		var count 		= 0;
  		var thumbs 		= '';
  		var slider 		= false;
  		$slides.each(function(index){
  			var $slide 	= $(this);
  			var thumb 	= $slide.find('img').attr('data-thumb');
  			thumbs += '<li'+ ( index == 0 ? ' class="active"' : '' ) +' data-index="'+ index +'"><div><img src="'+ thumb +'" class="img-responsive" /></div></li>';
  			$slide
  				.attr('data-index',index)
  				.find('img').removeAttr('data-thumb');
  			count++;
  		});
  		if( count > 1 ){
  			$thumbs
  				.removeClass('hidden')
  				.find('ul').html( thumbs );
  			$thumbs = $thumbs.find('li');
  			var active = function(){
  				var index = $slider.find('.active').find('.slide').attr('data-index');
  				$thumbs.removeClass('active').filter('[data-index="'+ index +'"]').addClass('active');
  			};
  			slider = $slider.owlCarousel({
	        css: true,
	        loop: true,
	        items: 1,
	        responsiveClass: true,
	        smartSpeed: 800,
	        nav: false,
	        dots: false,
	        navText: ["", ""]
  			});
        slider.on('changed.owl.carousel', function(event){
          setTimeout(function(){
            active();
          },200);
        });
        $thumbs.on('click',function(){
        	var index = $(this).attr('data-index');
        	slider.trigger('to.owl.carousel',[index, 800, true]);
        	return false;
        });
  		}
  	});
  };
  init();
})();

/* map */
function initialize() {
  var $map        = $('#pageMap');
  var latitude    = $map.attr('data-latitude');
  var longitude   = $map.attr('data-longitude');
  var zoom        = parseInt($map.attr('data-zoom'),10);
  // var pin         = $map.attr('data-pin');
  var options     = {
      zoom: zoom,
      center:         new google.maps.LatLng(latitude, longitude),
      mapTypeId:      google.maps.MapTypeId.ROADMAP,
      scrollwheel:    false           
  };
  var map = new google.maps.Map($map[0], options);
  var marker = new google.maps.Marker({
      position: new google.maps.LatLng(latitude, longitude),
      map: map,
      // icon: pin
  });
  var resize = function(){
      var center = map.getCenter();
      google.maps.event.trigger(map,'resize');
      map.setCenter(center);
  };
  google.maps.event.addDomListener(window,'resize', function(){
      resize();
  });
}

(function() {
	 var $modal  = $('#modal-recall');
    if( $modal.length == 0 ) return false;
    var $btn    = $('.btn-recall');
    var $link_close = $modal.find('.link-close');
    var show = function(){
        $modal.fadeIn();
    };
    var hide = function(){
        $modal.fadeOut(function(){
            $modal.find('input[type="text"],input[type="email"]').val('').end()
            .find('.errorMessage').hide().end()
            .find('.btn-submit').attr('disabled',true);
        });
    };
    $btn.on('click',function(){
        show();
        return false; 
    });
    $link_close.on('click',function(){
        hide();
        return false; 
    });
})();
/* navigation */
(function() {
	var $header = $('.header');
	var scrollTo = function(target){
		var offset = isNaN(parseInt(target,10)) ? ($(target).offset().top - $header.height()) : target;
  	$('body').animate({
  		scrollTop: offset
  	},600);
	};
	var init = function(){
		var $nav 		= $('.nav-main').find('a');
		$nav.each(function(){
			var $link 	= $(this);
			var target 	= $link.attr('href');
			var $target = $(target);
			if( $target.length > 0 ){
				var waypoint = new Waypoint({
        	element: $target[0],
        	handler: function(direction) {
        		$nav.parent('li').removeClass('active');
        		if( direction == 'down' ){
        			$link.parent('li').addClass('active');
        		}else{
        			$link.parent('li').prev('li').addClass('active');
        		}
        	},
        	offset: function(){
						return $header.height();
        	}
        });
        $link.on('click',function(){
        	scrollTo( $target );
        	return false;
        });
			}
		});
	};
	init();
	$('[data-scrollto]').on('click',function(){
		var target = $(this).attr('data-scrollto');
		scrollTo( target );
		return false;
	});

	var scroll = function(){
		var y = $(this).scrollTop();
		if( y >= 200 ){
			$('body').addClass('header-compact');
		}else{
			$('body').removeClass('header-compact');
		}
	};
	$(window).on('scroll',function(){
		scroll();
	});

	scroll();

})();

// (function() {
// 	$(document).delegate('.toggle-item .btn-toggle','click',function(){
//         var $btn    = $(this);
//         var $item   = $btn.parents('.toggle-item');
//         $item.toggleClass('toggled').find('.toggle-item-in').slideToggle(300);
//         return false; 
//     });
// })();
//# sourceMappingURL=interface.js.map
