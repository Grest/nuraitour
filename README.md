## Getting started

- Install dependencies: `npm install --global gulp-cli bower`
- Run `bower install` to install frontend dependencies
- Run `npm install` to install dev dependencies
- Run `gulp serve` to preview and watch for changes
- Run `bower install --save <package>` to install frontend dependencies
- Run `gulp` to run the tests in the browser


## App structure

```
app
|- fonts
|- images
|- samples
|- scripts
|- styles
|- views
test
```
