const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const autoprefixer = require('autoprefixer');
const assets = require('postcss-assets');
const browserSync = require('browser-sync');
const del = require('del');
const sprites = require('postcss-sprites').default;
const spritesUpdateRule = require('postcss-sprites').updateRule;
const assemble = require('assemble');
const postcss = require('postcss');
const pxtorem = require('postcss-pxtorem');
const syntaxScss = require('postcss-scss');
const $ = gulpLoadPlugins();
const reload = browserSync.reload;

const app = assemble();

var paths = {
  src: {
    path: 'src',
    fonts: 'src/fonts',
    images: 'src/img',
    samples: 'src/samples',
    scripts: 'src/scripts',
    styles: 'src/styles',
    views: 'src/views'
  },
  temp: {
    path: '.temp',
    images: '.temp/img',
    scripts: '.temp/js',
    styles: '.temp/css'
  },
  build: {
    path: 'build',
    fonts: 'build/fonts',
    images: 'build/img',
    samples: 'build/samples',
    scripts: 'build/js',
    styles: 'build/css'
  }
};

gulp.task('dev:loadViews', (cb) => {
  app.partials( paths.src.views + '/partials/*.hbs');
  app.layouts( paths.src.views + '/layouts/*.hbs');
  app.pages( paths.src.views + '/*.hbs');
  cb();
});

gulp.task('dev:views', ['dev:loadViews'], () => {
  return app.toStream('pages')
    .pipe(app.renderFile())
    .pipe($.extname())
    .pipe(app.dest( paths.temp.path ));
});

gulp.task('dev:styles', () => {
  return gulp.src( paths.src.styles + '/*.scss')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.'],
    }).on('error', $.sass.logError))
    .pipe($.postcss([
      autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}),
      assets({
        basepPath: paths.src.path +'/',
        cachebuster: true,
        loadPaths: [ paths.src.images + '/'],
      }),
      sprites({
        stylesheetPath: './' + paths.temp.styles,
        spritePath: './' + paths.temp.images,
        filterBy(image) {
          // Allow only png files
          if (!/\/?sprites\/.*\.png$/.test(image.url)) {
            return Promise.reject();
          }
          return Promise.resolve();
        },
        hooks: {
          onUpdateRule(rule, token, image) {
            // Use built-in logic for background-image & background-position
            spritesUpdateRule(rule, token, image);
            ['width', 'height'].forEach((prop) => {
              rule.insertAfter(rule.last, postcss.decl({
                prop,
                value: image.coords[prop] + 'px',
              }));
            });
          }
        }
      }),
      pxtorem({
        propWhiteList: [
          'font', 'font-size', 'line-height',
          'margin', 'margin-top', 'margin-right', 'margin-bottom', 'margin-left',
          'padding', 'padding-top', 'padding-right', 'padding-bottom', 'padding-left',
          'left', 'right', 'top', 'bottom',
          'width', 'height', 'min-width', 'min-width', 'max-height', 'min-height',
          'border-radius', 'border-top-left-radius', 'border-top-right-radius', 'border-bottom-left-radius', 'border-bottom-right-radius'
        ],
      })
    ]))
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest( paths.temp.styles ))
    .pipe(reload({stream: true}));
});

gulp.task('dev:scripts', () => {
  return gulp.src( paths.src.scripts + '/partials/*.js')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.concat('interface.js'))
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest( paths.temp.scripts ))
    .pipe(reload({stream: true}));
});

// ******************************************************************************

gulp.task('build:views', ['dev:styles', 'dev:scripts'], () => {
  return gulp.src( paths.temp.path +'/*.html')
    .pipe($.eol("\r\n"))
    .pipe($.useref({searchPath: [ paths.temp.path, paths.src.path, '.']}))
    // .pipe($.if('*.js', $.uglify()))
    // .pipe($.if('*.css', $.cssnano()))
    .pipe(gulp.dest(paths.build.path));
});

gulp.task('minify', () => {
  return gulp.src([ paths.build.styles +'/*.css', paths.build.scripts +'/*.js' ])
    .pipe($.if('*.js', $.uglify() ))
    .pipe($.if('*.css', $.cssnano({autoprefixer: false})))
    .pipe($.rename({suffix:'.min'}))
    .pipe($.if('*.js', gulp.dest(paths.build.scripts)))
    .pipe($.if('*.css', gulp.dest(paths.build.styles)))
});

gulp.task('build:images', () => {
  return gulp.src([ paths.src.images +'/*.png', paths.src.images +'/*.jpg' ])
    .pipe($.imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{cleanupIDs: false}]
    }))
    .pipe(gulp.dest( paths.build.images ));
});
gulp.task('build:copyimages', () => {
  return gulp.src([ paths.src.images +'/*.png', paths.src.images +'/*.jpg', paths.src.images +'/*.svg', paths.temp.images +'/*.png' ])
    .pipe(gulp.dest( paths.build.images ));
});

gulp.task('build:samples', () => {
  return gulp.src([ paths.src.samples +'/*.png', paths.src.samples +'/*.jpg' ] )
    .pipe($.imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{cleanupIDs: false}]
    }))
    .pipe(gulp.dest( paths.build.samples ));
});
gulp.task('build:copysamples', () => {
  return gulp.src([ paths.src.samples +'/*.png', paths.src.samples +'/*.jpg', paths.src.samples +'/*.svg' ] )
    .pipe(gulp.dest( paths.build.samples ));
});

gulp.task('build:fonts', () => {
  return gulp.src( paths.src.fonts + '/*' )
    .pipe(gulp.dest( paths.build.fonts ));
});


// ******************************************************************************
gulp.task('clean', del.bind(null, [ paths.temp.path, paths.build.path ] ));

gulp.task('default', ['dev:views'], () => {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: [paths.temp.path, paths.src.path],
      routes: {
        '/bower_components': 'bower_components',
      }
    }
  });

  gulp.watch([
    paths.src.scripts + '/**/*.js',
    paths.src.images + '/*',
    paths.src.samples + '/*',
    paths.temp.fonts +'/*',
    paths.temp.path + '/*.html',
  ]).on('change', reload);

  gulp.watch( paths.src.styles + '/**/*.scss', ['dev:styles']);
  gulp.watch( paths.src.scripts + '/partials/*.js', ['dev:scripts']);
  gulp.watch( paths.src.views + '/**/*.hbs', ['dev:views']);
});

gulp.task('notify', () => {
  return gulp.src( paths.src.path )
    .pipe($.notify('билд создан'));
});

gulp.task('process', ['dev:views', 'build:views', 'build:images', 'build:samples', 'build:copyimages', 'build:copysamples', 'build:fonts'], () => {
  gulp.start('minify');
  gulp.start('notify');
});

gulp.task('build', ['clean'], () => {
  gulp.start('process');
});
