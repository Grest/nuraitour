/* gallery */
(function() {
  var init = function(){
  	$('.gallery').each(function(){
  		var $gallery 	= $(this);
  		var $slider 	= $gallery.find('.carousel');
  		var $slides 	= $slider.find('.slide');
  		var $thumbs 	= $gallery.find('.thumbs');
  		var count 		= 0;
  		var thumbs 		= '';
  		var slider 		= false;
  		$slides.each(function(index){
  			var $slide 	= $(this);
  			var thumb 	= $slide.find('img').attr('data-thumb');
  			thumbs += '<li'+ ( index == 0 ? ' class="active"' : '' ) +' data-index="'+ index +'"><div><img src="'+ thumb +'" class="img-responsive" /></div></li>';
  			$slide
  				.attr('data-index',index)
  				.find('img').removeAttr('data-thumb');
  			count++;
  		});
  		if( count > 1 ){
  			$thumbs
  				.removeClass('hidden')
  				.find('ul').html( thumbs );
  			$thumbs = $thumbs.find('li');
  			var active = function(){
  				var index = $slider.find('.active').find('.slide').attr('data-index');
  				$thumbs.removeClass('active').filter('[data-index="'+ index +'"]').addClass('active');
  			};
  			slider = $slider.owlCarousel({
	        css: true,
	        loop: true,
	        items: 1,
	        responsiveClass: true,
	        smartSpeed: 800,
	        nav: false,
	        dots: false,
	        navText: ["", ""]
  			});
        slider.on('changed.owl.carousel', function(event){
          setTimeout(function(){
            active();
          },200);
        });
        $thumbs.on('click',function(){
        	var index = $(this).attr('data-index');
        	slider.trigger('to.owl.carousel',[index, 800, true]);
        	return false;
        });
  		}
  	});
  };
  init();
})();
