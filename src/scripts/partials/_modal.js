(function() {
	 var $modal  = $('#modal-recall');
    if( $modal.length == 0 ) return false;
    var $btn    = $('.btn-recall');
    var $link_close = $modal.find('.link-close');
    var show = function(){
        $modal.fadeIn();
    };
    var hide = function(){
        $modal.fadeOut(function(){
            $modal.find('input[type="text"],input[type="email"]').val('').end()
            .find('.errorMessage').hide().end()
            .find('.btn-submit').attr('disabled',true);
        });
    };
    $btn.on('click',function(){
        show();
        return false; 
    });
    $link_close.on('click',function(){
        hide();
        return false; 
    });
})();