/* navigation */
(function() {
	var $header = $('.header');
	var scrollTo = function(target){
		var offset = isNaN(parseInt(target,10)) ? ($(target).offset().top - $header.height()) : target;
  	$('body').animate({
  		scrollTop: offset
  	},600);
	};
	var init = function(){
		var $nav 		= $('.nav-main').find('a');
		$nav.each(function(){
			var $link 	= $(this);
			var target 	= $link.attr('href');
			var $target = $(target);
			if( $target.length > 0 ){
				var waypoint = new Waypoint({
        	element: $target[0],
        	handler: function(direction) {
        		$nav.parent('li').removeClass('active');
        		if( direction == 'down' ){
        			$link.parent('li').addClass('active');
        		}else{
        			$link.parent('li').prev('li').addClass('active');
        		}
        	},
        	offset: function(){
						return $header.height();
        	}
        });
        $link.on('click',function(){
        	scrollTo( $target );
        	return false;
        });
			}
		});
	};
	init();
	$('[data-scrollto]').on('click',function(){
		var target = $(this).attr('data-scrollto');
		scrollTo( target );
		return false;
	});

	var scroll = function(){
		var y = $(this).scrollTop();
		if( y >= 200 ){
			$('body').addClass('header-compact');
		}else{
			$('body').removeClass('header-compact');
		}
	};
	$(window).on('scroll',function(){
		scroll();
	});

	scroll();

})();
