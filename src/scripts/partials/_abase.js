/* base */
(function() {
	$(document).delegate('.toggle-item .btn-toggle','click',function(){
        var $btn    = $(this);
        var $item   = $btn.parents('.toggle-item');
        $item.toggleClass('toggled').find('.toggle-item-in').slideToggle(300);
        return false; 
    });
})();