/* map */
function initialize() {
  var $map        = $('#pageMap');
  var latitude    = $map.attr('data-latitude');
  var longitude   = $map.attr('data-longitude');
  var zoom        = parseInt($map.attr('data-zoom'),10);
  // var pin         = $map.attr('data-pin');
  var options     = {
      zoom: zoom,
      center:         new google.maps.LatLng(latitude, longitude),
      mapTypeId:      google.maps.MapTypeId.ROADMAP,
      scrollwheel:    false           
  };
  var map = new google.maps.Map($map[0], options);
  var marker = new google.maps.Marker({
      position: new google.maps.LatLng(latitude, longitude),
      map: map,
      // icon: pin
  });
  var resize = function(){
      var center = map.getCenter();
      google.maps.event.trigger(map,'resize');
      map.setCenter(center);
  };
  google.maps.event.addDomListener(window,'resize', function(){
      resize();
  });
}
